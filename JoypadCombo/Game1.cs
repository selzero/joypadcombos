
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using HologramSpriteManager;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace JoypadCombo
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            SpriteManager.ContentShell = Content;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            SpriteManager.GameTime = 0;

            base.Initialize();
        }

        ComboDefinitions AllCombos;
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteManager.spriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here
            string sComboText = System.IO.File.ReadAllText(@".\Content\Specs\combos.json");
            AllCombos = JsonConvert.DeserializeObject<ComboDefinitions>(sComboText);
            JoypadStates = new List<GamePadStateData>();
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        List<GamePadStateData> JoypadStates;
        float fComboDuration = 600;


        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            SpriteManager.GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            ComboNode NewNode = new ComboNode(GamePad.GetState(PlayerIndex.One), SpriteManager.GameTime);
            if (!NewNode.Empty)
                JoypadStates.Add(new GamePadStateData(NewNode));

            if (GamePad.GetState(PlayerIndex.One).Buttons.X == ButtonState.Pressed)
            {
                int a = 0;
            }


            EvaluateCombo();

            base.Update(gameTime);
        }

        float fLastTime = 0;
        void EvaluateCombo()
        {
            //remove olds from stack

            List<int> IndexToRemove = new List<int>();

            if (JoypadStates.Count > 3)
            {
                int a = 0;
            }

            if (JoypadStates.Count > 0)
            {
                int LastChange = 0;
                for (int i = 0; i < JoypadStates.Count; i++)
                {
                    GamePadStateData NextData = JoypadStates[i];
                    if (NextData.State.fTime < SpriteManager.GameTime - fComboDuration)
                    {
                        IndexToRemove.Add(i);
                    }
                    else
                    {
                        if (i != LastChange && NextData.State.Compare(JoypadStates[LastChange].State))
                        {
                            IndexToRemove.Add(LastChange);

                        }
                        LastChange = i;
                    }
                }
            }
            for (int i = IndexToRemove.Count - 1; i > -1; i--)
            {
                JoypadStates.RemoveAt(IndexToRemove[i]);
            }


            List<ComboNode> CollectedNodes = new List<ComboNode>();

            //loop through combos and check
            for (int iComboCounter = 0; iComboCounter < AllCombos.Combos.Count; iComboCounter++)
            {

                int iDefinitionCounter = 0;
                ComboDefinition CurrentDefinition = AllCombos.Combos[iComboCounter];
                ComboNode FirstNode = CurrentDefinition.ComboStages[iDefinitionCounter];

                //loop through states
                for (int iStateCounter = 0; iStateCounter < JoypadStates.Count; iStateCounter++)
                {
                    //Console.WriteLine("combo: " + iComboCounter + " State count: " + JoypadStates.Count + " state: " + iStateCounter ); 
                    //try and find one matching first node
                    ComboNode CurrentStateNode = JoypadStates[iStateCounter].State;
                    if (CurrentStateNode.Compare(FirstNode) && CurrentDefinition.ComboStages.Count <= JoypadStates.Count - iStateCounter)
                    {
                        CollectedNodes = new List<ComboNode>();

                        ComboNode CurrentNode = CurrentDefinition.ComboStages[iDefinitionCounter];
                        //the current state matches the first node, run down and try to find a sequence match
                        //ComboNode CurrentNode = CurrentDefinition.ComboStages[iDefinitionCounter];
                        //ComboNode NextNode = CurrentDefinition.ComboStages[iDefinitionCounter + 1];
                        for (int iInternalStateCounter = iStateCounter; iInternalStateCounter < JoypadStates.Count; iInternalStateCounter++)
                        {
                            CurrentStateNode = JoypadStates[iInternalStateCounter].State;
                            ComboNode NextNode = CurrentDefinition.ComboStages[iDefinitionCounter];

                            if (CurrentStateNode.Compare(NextNode))
                            {
                                //we take another step
                                if (iDefinitionCounter > 1)
                                {
                                    int a = 1;
                                }
                                CollectedNodes.Add(CurrentStateNode);
                                iDefinitionCounter++;
                                CurrentNode = NextNode;

                            }
                            else
                            {
                                //leave, the sequence didnt work
                                break;
                            }

                        }


                        if (CollectedNodes.Count == CurrentDefinition.ComboStages.Count)
                        {
                            //check the timing
                            float fTimeTaken = CollectedNodes[CollectedNodes.Count - 1].fTime - CollectedNodes[0].fTime;

                            if (fTimeTaken <= CurrentDefinition.ComboTime)
                            {
                                Console.WriteLine("COMBO SUCCESS: " + CurrentDefinition.ComboName);
                                JoypadStates = new List<GamePadStateData>();
                                iDefinitionCounter = 0;
                            }
                            else
                            {

                                Console.WriteLine("Executed: " + CurrentDefinition.ComboName + " in " + fTimeTaken + " seconds, too late");
                            }
                        }
                    }
                }
            }
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // TODO: Add your drawing code here
            SpriteManager.spriteBatch.Begin();


            SpriteManager.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
